/*
 * mdb@insaneminds.org (c) 2004, All rights reserved.
 * 
 * Nothing can be reproduced in whole or in part without the prior
 * written permission of the author.
 * Permission to link to this code from the internet is hereby granted.
 */

#ifndef _MAIN_C
# define _MAIN_C

# ifndef _CRAZYNET_H
#  include "crazynet.h"
# endif

char *attualivoti, *hostname, *password;

void fuckhead (int sig) 
{
   warnx("signal %u received..\n", sig);
   if (attualivoti)
     free(attualivoti);
   exit(-1);
}

void usage (const char *me) 
{
   fprintf(stderr, 
	   "ce ancora qualche if da levare...", me);
   exit(0);
}

int main () 
{   
   printf( 
	   "[*] venividivici :: mdb's voter [*]\n");      
   printf( 
	   "[*] Nobody Can Hack My Mind [*]\n\n");
	   
   struct sockaddr_in sa;
   int sockfd;
   
   signal (SIGHUP, SIG_IGN);
   signal (SIGINT, fuckhead);
   signal (SIGQUIT, fuckhead);
   signal (SIGTERM, fuckhead);
   signal (SIGSEGV, fuckhead);
   signal (SIGBUS, fuckhead);

   printf( 
	   "[*] Controllo possibilita' di creare una connessione\n");
   if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
     errx(1, "socket(): %s\n", strerror(errno));
   printf( 
	   "[*] Preparazione headers HTTP/1.1\n");      
   memset((void *)&sa, 0, sizeof(sa));
   sa.sin_family = AF_INET;
   sa.sin_port = htons(DESTINATION_PORT);
   sa.sin_addr.s_addr = resolve(DESTINATION_HOST);
   memset((void *)&sa.sin_zero, 0, sizeof(sa.sin_zero));
   printf( 
	   "[*] Apertura connessione socket verso l'hostname: www.fotomodelladellanno.com\n");      
   if ((connect(sockfd, (struct sockaddr *)&sa, sizeof(struct sockaddr))) < 0)
     errx(1, "connect(): %s\n", strerror(errno));
   printf( 
	   "[*] Invio headers HTTP/1.1 e dei dati\n");      
	attualivoti = "391";

	swrite(sockfd,
	  "POST /fotomodella/default.asp?dove=votare HTTP/1.0\r\n"

	  "User-Agent: %s %s\r\n"
	  "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,video/x-mng,image/png,image/jpeg,image/gif;q=0.2,text/css,*/*;q=0.1\r\n"
	  "Content-Type: application/x-www-form-urlencoded\r\n"
	  "idadd=33&voti2=%s\r", 
	  CLIENTNAME, CLIENTVER, attualivoti);
	  
   printf( 
	   "[*] Chiusura connessione socket\n");      
   close(sockfd);
   
      printf( 
	   "[*] Voto correttamente eseguito\n");      

   return(0);
}

#endif
