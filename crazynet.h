#ifndef _CRAZYNET_H
# define _CRAZYNET_H

# include <sys/types.h>
# include <sys/socket.h>

# include <netinet/in.h>
# include <arpa/inet.h>
# include <netdb.h>

# include <stdio.h>
# include <stdarg.h>
# include <stdlib.h>
# include <string.h> 

# include <signal.h>

# include <unistd.h>

# include <err.h>
# include <errno.h>

extern int errno;

# ifndef CLIENTNAME
#  define CLIENTNAME ("Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.0.7)")
# endif

# ifndef CLIENTVER
#  define CLIENTVER ("Gecko/20060909 Firefox/1.5.0.7")
# endif

# ifndef BUFSIZE
#  define BUFSIZE 1024
# endif

# ifndef DESTINATION_HOST
#  define DESTINATION_HOST ("www.hippo.it")
# endif

# ifndef DESTINATION_PORT
#  define DESTINATION_PORT 80
# endif

# ifndef _RESOLVE_C
u_int32_t resolve(const char *);
# endif

# ifndef _SOCKET_C
int swrite(int, const char *, ...);
# endif

#endif
