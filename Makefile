#
# mdb@insaneminds.org (c) 2004, All rights reserved.
# 
# Nothing can be reproduced in whole or in part without the prior
# written permission of the author.
# Permission to link to this code from the internet is hereby granted.
#

CC = cc
BIN = venividivici
SOURCE = main.c resolve.c socket.c
OBJ = main.o resolve.o socket.o
LIB =
FLAGS = -W -Wall

all: objs venividivici

objs:	   
	   $(CC) $(FLAGS) $(SOURCE) -c

venividivici:	   
	   $(CC) $(FLAGS) $(OBJ) -o $(BIN) $(LIB)

clean:	   	 	  
	   rm -f *.o *.BAK *.core *~ $(BIN)

