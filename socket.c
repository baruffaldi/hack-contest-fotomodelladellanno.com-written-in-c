/*
 * mdb@insaneminds.org (c) 2004, All rights reserved.
 * 
 * Nothing can be reproduced in whole or in part without the prior
 * written permission of the author.
 * Permission to link to this code from the internet is hereby granted.
 */

#ifndef _SOCKET_C
# define _SOCKET_C

# ifndef _CRAZYNET_H
#  include "crazynet.h"
# endif

int swrite (int socket, const char *fmt, ...)
{
   int ret;
   char buf[BUFSIZE];
   va_list args;
   
   va_start(args, fmt);
   vsnprintf(buf, BUFSIZE, fmt, args);
   va_end(args);
   ret = write(socket, (void *)&buf, strlen(buf));
   return(ret);
}

#endif
