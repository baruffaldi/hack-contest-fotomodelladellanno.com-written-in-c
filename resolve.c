/*
 * mdb@insaneminds.org (c) 2004, All rights reserved.
 * 
 * Nothing can be reproduced in whole or in part without the prior
 * written permission of the author.
 * Permission to link to this code from the internet is hereby granted.
 */

#ifndef _RESOLVE_C
# define _RESOLVE_C

# ifndef _CRAZYNET_H
#  include "crazynet.h"
# endif

extern int h_errno;

u_int32_t resolve (const char *name) 
{
   struct hostent *h;
   
   if (!(h = gethostbyname(name)))
     errx(1, "gethostbyname(): %s", hstrerror(h_errno));
   
   return(*((u_int32_t *)h->h_addr));   
}

#endif
